# 如何在开发板运行ets工程

> 开发板为润和大禹200系列

### 1. 新建工程

模板选择最后一个[Standard]Empty Ability，这会使用OpenHarmony的SDK，其他的则会使用HarmonyOS的SDK



### 2. 配置签名

运行到开发板上的hap都需要签名，参考：https://www.openharmony.cn/pages/00090003/#%E7%94%9F%E6%88%90%E5%AF%86%E9%92%A5%E5%92%8C%E8%AF%81%E4%B9%A6%E8%AF%B7%E6%B1%82%E6%96%87%E4%BB%B6

后续不同的工程只需要使用不同的bundle name重新生成profile文件即可。



### 3. 连接开发板

参考下面的链接连接开发板并安装驱动：

https://gitee.com/hihope_iot/docs/tree/master/HiHope_DAYU200



### 4. 检查hdc_std版本

- hdc_std位于OpenHarmony SDK目录的toolchains目录下。

- 在cmd中切换到该目录，执行``hdc_std list targets -v``，如果开发板连接成功会显示设备的名称，如果没有成功请检查设备是否正确的连接到了pc。

- hdc_std版本使用``hdc_std -v``查看

- 执行``hdc_std shell``命令，如果命令无响应或者报错则说明hdc的版本不匹配。hdc的服务端在开发板的OpenHarmony系统内，客户端则是我们使用的hdc_std.exe，两个版本需要保持一致，至少版本不能相差过大。

- 不一致的话需要使用最新的hdc_std.exe，如果shell命令还是不行则需要烧录最新的OpenHarmony系统至开发板内。

- 最新的hdc_std.exe可以通过DevEco更新toolchains来获取，如果没有更新则去daily builds上下载最新的ohos-sdk：

  http://ci.openharmony.cn/dailybuilds

- 解压并获取toolchains下的hdc_std.exe

- 烧录的系统镜像也需要去daily builds上下载最新的dayu200镜像，如何烧录请参考：https://gitee.com/hihope_iot/docs/tree/master/HiHope_DAYU200/%E7%83%A7%E5%86%99%E5%B7%A5%E5%85%B7%E5%8F%8A%E6%8C%87%E5%8D%97

- 开发板的hdc版本则可以在shell命令或者连接串口后输入``hdcd -v``查看



### 5. 安装hap包

- hdc就绪后先使用DevEco的build hap功能打出hap包，并在entry的build目录下找到签名的hap包，

- 通过下面的命令安装到开发板内：

  ``hdc_std install xxxxxxx\entry-debug-standard-ark-signed.hap``

  安装包的路径需要自行替换



### 6. 查看日志

- 执行``hdc_std shell``命令
- 使用``hilog | grep 'tag'``来筛选自己感兴趣的日志
- 其他选项参考https://gitee.com/openharmony/hiviewdfx_hilog/blob/master/README_zh.md